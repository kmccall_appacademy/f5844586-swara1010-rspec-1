def add(first_num, second_num)
  total_sum = first_num + second_num
  total_sum
end

def subtract(element_one, element_two)
  total_sub = element_one - element_two
  total_sub
end

def sum(arr_numbers)
   if arr_numbers == []
        return 0
   end
     arr_numbers.reduce(:+)
end

def multiply(first_num, second_num)
   total_multiply = first_num * second_num
   total_multiply
end

def power(first_num, second_num)
   total_power = first_num ** second_num
   total_power
end

def factorial(number)
  factorial_arr = []
  if number == 0
    return 0
  end
  (1..number).each do |el|
    factorial_arr << el
  end
   factorial_arr.reduce(:*)
end
