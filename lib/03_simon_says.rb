def echo(word)
  word.downcase
end

def shout(word)
  word.upcase
end

def repeat(word, time=2)
  repeat_words = (word.split) * time
  repeat_words.join(" ")
end

def start_of_word(word, num)
  word_arr = word.chars
  final_arr = []
  word_arr.each_with_index do |char, idx|
    if  idx < num
      final_arr << char
    end
  end
  final_arr.join
end

def first_word(str)
  str_to_word = str.split
  str_to_word[0]
end

def titleize(string)
  capital_string = string.split
  string_arr = []
  little_words = ["and", "the", "over"]
  capital_string.each do |word|
    if little_words.include?(word)
      string_arr << word.downcase
    else
      string_arr << word.capitalize
    end
  end
  string_arr.join(" ")
end
